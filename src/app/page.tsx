"use client";
import { useState } from "react";
import List from "./components/List";
import TaskInput from "./components/TaskInput";

interface tasksArrType {
  tasks: string[]
  setTasks: string
}

const Home: React.FC<tasksArrType> = () => {
  const [tasks, setTasks] = useState([]);

  return (
      <div className="flex items-center justify-center">
        <div className="text-center">
          <h1 className="text-8xl my-4 text-white">To-do list</h1>
          <TaskInput tasks={tasks} setTasks={setTasks} />
          <div className="text-start">
          <List tasks={tasks} setTasks={setTasks} />
          </div>
        </div>
      </div>

  );
};

export default Home;
