import React from "react";

interface InputType {
  tasks: string[];
  setTasks: any;
}

const Input: React.FC<InputType> = ({ tasks, setTasks }) => {
  const [newTask, setNewTask] = React.useState<string>("");
  const onClickAdd = () => {
    if (newTask !== "") {
      setTasks([...tasks, newTask]);
      setNewTask("");
    }
  };
  const onPressEnter = (event: { key: string }) => {
    if (event.key === "Enter") {
      setTasks([...tasks, newTask]);
      setNewTask("");
    }
  };

  console.log(onClickAdd);
  return (
    <div className="flex items-center my-4">
      <input
        type="text"
        value={newTask}
        onChange={(e) => setNewTask(e.target.value)}
        onKeyDown={onPressEnter}
        className="
          w-[44rem]
          h-20
          border-[1px] 
          border-[black] 
          rounded-2xl 
          focus:outline-none
          px-1
          py-[1px]
          text-5xl
        "
        placeholder="Make new task?"
      />
      <button
        className="
          bg-pink-400 
          rounded-xl
          ml-1
          text-4xl
          w-[92px]
          h-[5rem]
          cursor-pointer
          hover:bg-pink-300
          hover:text-white
        "
        onClick={onClickAdd}
      >
        Add
      </button>
    </div>
  );
};

export default Input;
