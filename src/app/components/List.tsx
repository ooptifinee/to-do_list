import React, { useState } from 'react';

interface ListProps {
  tasks: string[];
  setTasks: any;
}

const List: React.FC<ListProps> = ({ tasks, setTasks }) => {
  const [editableIndex, setEditableIndex] = useState<number | null>(null);
  const [editedText, setEditedText] = useState<string>('');

  const handleEdit = (index: number, currentText: string) => {
    setEditableIndex(index);
    setEditedText(currentText);
  };

  const handleConfirmEdit = (index: number) => {
    const updatedTasks = [...tasks];
    updatedTasks[index] = editedText;
    setTasks(updatedTasks);
    setEditableIndex(null);
    setEditedText('');
  };

  const handleCancelEdit = () => {
    setEditableIndex(null);
    setEditedText('');
  };

  const handleDelete = (index: number) => {
    const updatedTasks = tasks.filter((_, i) => i !== index);
    setTasks(updatedTasks);
  };

  const handleEnterConfirm = (index: number, e: {key: string}) => {
    if (e.key === 'Enter') {
      handleConfirmEdit(index);
    } else if (e.key === "Escape") {
      handleCancelEdit()
    }
  };

  return (
    <ul className="
      bg-[#33414b] 
      w-[50rem] 
      min-h-[12rem] 
      py-2
      pl-2
      rounded-xl
    ">
      {tasks.map((event, index) => (
        <div className="flex items-center" key={index}>
          {editableIndex === index ? (
            <>
              <input
                type="text"
                value={editedText}
                onChange={(e) => setEditedText(e.target.value)}
                onKeyDown={(e) => handleEnterConfirm(index, e)}
                className="text-4xl text-black my-6 mx-2 p-2 bg-gray-200 rounded-lg"
              />
              <button
                className="mr-2 w-28 h-12 bg-green-500 rounded-lg text-2xl"
                onClick={() => handleConfirmEdit(index)}
              >
                Confirm
              </button>
              <button
                className="w-24 h-12 bg-red-500 rounded-lg text-2xl"
                onClick={handleCancelEdit}
              >
                Cancel
              </button>
            </>
          ) : (
            <>
              <li className="text-4xl text-white my-6 mx-2 p-2 bg-gray-600 rounded-lg">
                {event}
              </li>
              <button
                className="mr-2 w-16 h-12 bg-green-500 rounded-lg text-2xl"
                onClick={() => handleEdit(index, event)}
              >
                Edit
              </button>
              <button
                className="w-16 h-12 bg-red-500 rounded-lg text-2xl"
                onClick={() => handleDelete(index)}
              >
                Del
              </button>
            </>
          )}
        </div>
      ))}
    </ul>
  );
};

export default List;
