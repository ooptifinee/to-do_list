interface ListProps {
  tasks: string[];
}

const List: React.FC<ListProps> = ({ tasks }) => {
  const taskItem: string[] = tasks

  
  return (
    <ul
      className="
    bg-[#33414b] 
    w-[50rem] 
    min-h-[12rem] 
    py-2
    pl-2
    rounded-xl
    "
    >
      {taskItem.map((event, index) => (
        <li className="text-4xl text-white my-6 mx-2" key={index}>
          {event}
        </li>
      ))}
    </ul>
  );
};

export default List;
